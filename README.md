# Ansible Playbooks

##*Dependencies*

1. ansible 2.6.2 or above
2. ~/.ssh/config set with remote hosts trying to access
3. /etc/ansible/hosts file config with hostname you are targeting


### 1 Kubernetes Install for Raspberry Pi (Debian Raspian)

1. clone repo
2. cd /path/to/cloned/repo
3. ansible-playbook pi-master-k8s.yml --extra-vars "master_ip=192.168.0.2" 

[credit to](https://gist.github.com/alexellis/fdbc90de7691a1b9edb545c17da2d975)